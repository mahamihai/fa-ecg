#include <iostream>
#include <fstream>

#include"Profiler.h"
//usually the bootom up works better than the top down
//bottom up is more efficient than top down as the number of numbers increase
//the worst case is when the array is already sorted
//
Profiler profiler("heap");
using namespace std;
int sir1[10000] ;
int sir2[10000];
int n;
int max_size = 10000;
int sorted[10000];
int numbers;//size
void heapify(int index)
{
	int pos = index;
	if ((index * 2 + 1)< numbers)
	{
		if (sir1[index * 2 + 1] > sir1[pos])
		{
			pos = index * 2 + 1;

		}
		profiler.countOperation("Bottom Comparisons", numbers, 1);

	}
	if ((index * 2 + 2) < numbers)
	{
		if (sir1[index * 2 + 2] > sir1[pos])
		{
			pos = index * 2 + 2;

		}
		profiler.countOperation("Bottom Comparisons", numbers, 1);
	}
	if (pos != index)

	{
		
		profiler.countOperation("Bottom Assignations", numbers, 3);
		int aux = sir1[index];
		sir1[index] = sir1[pos];
		sir1[pos] = aux;
		heapify(pos);

	}

	
}
void heap_bottom()
{
	for (int i = numbers/2; i>-1 ; i--)
	{


		heapify(i);
		
		
		
		}
}
void top_down()
{


	ifstream in1("in.txt");
	
	for (int i = 0; i < numbers; i++)
	{
			//read new nr
		int index = i;
		if (index > 0)
		{
			int found = 0;
			int j = index;
			while (!found && j>0)
			{
				j = (int)(j - 1) / 2;

				if (sir2[index] > sir2[j])
				{
					profiler.countOperation("Top Assignations", numbers, 3);
					int t = sir2[index];
					sir2[index] = sir2[j];
					sir2[j] = t;
					index = j;

				}
				
				else
				{
					found = 1;
				}
				profiler.countOperation("Top Comparisons", numbers, 1);
			}
		}

	}
}
void copy_arr()
{
	for (int i = 0; i < numbers; i++)
	{
		sir2[i] = sir1[i];
	}
}
void average()
{
	//this is the average case
	for (int j = 0; j < 5; j++)
	{


		for (int i = 100; i < max_size; i += 200)
		{
			numbers = i;

			FillRandomArray(sir1, i, 0, 20000, true, 0);
			copy_arr();
			top_down();
			heap_bottom();
			cout << "Ready";
			profiler.createGroup("Comparisons", "Top Comparisons", "Bottom Comparisons");
			profiler.createGroup("Assignations", "Top Assignations", "Bottom Assignations");
			profiler.addSeries("Bottom Operations", "Bottom Comparisons", "Bottom Assignations");
			profiler.addSeries("Top Operations", "Top Comparisons", "Top Assignations");
			profiler.createGroup("Operations", "Bottom Operations", "Top Operations");

		}
	}



}
void worst()
{
	for (int i = 100; i < max_size; i += 200)
	{
		numbers = i;

		FillRandomArray(sir1, i, 0, 20000, true, 1);
		copy_arr();
		top_down();
		heap_bottom();
		cout << "ready";


	}

	profiler.createGroup("Comparisons", "Top Comparisons", "Bottom Comparisons");
	profiler.createGroup("Assignations", "Top Assignations", "Bottom Assignations");
	profiler.addSeries("Bottom Operations", "Bottom Comparisons", "Bottom Assignations");
	profiler.addSeries("Top Operations", "Top Comparisons", "Top Assignations");
	profiler.createGroup("Operations", "Bottom Operations", "Top Operations");

}
void demo()
{
	sir1[0] = 2;
	sir1[1] = 5;
	sir1[2] = 1;
	sir1[3] = 13;
	sir1[4] = 21;
	sir1[5] = 41;
	sir1[6] = 30;
	sir1[7] = 17;
	sir1[8] = 9;
	numbers = 9;
	copy_arr();
	top_down();
	heap_bottom();

	profiler.createGroup("Comparisons", "Top Comparisons", "Bottom Comparisons");
	profiler.createGroup("Assignations", "Top Assignations", "Bottom Assignations");
	profiler.addSeries("Bottom Operations", "Bottom Comparisons", "Bottom Assignations");
	profiler.addSeries("Top Operations", "Top Comparisons", "Top Assignations");
	profiler.createGroup("Operations", "Bottom Operations", "Top Operations");

}
void heapsort()
{
	sir1[0] = 2;
	sir1[1] = 5;
	sir1[2] = 1;
	sir1[3] = 13;
	sir1[4] = 21;
	sir1[5] = 41;
	sir1[6] = 30;
	sir1[7] = 17;
	sir1[8] = 9;
	numbers = 9;
	heap_bottom();
	int index = numbers;
	for (int i = 0; i < index; i++)
	{
		
		sorted[i] = sir1[0];
		sir1[0] = sir1[numbers-1];
		numbers--;
		heapify(0);

		
	}
}


int main()
{
	
	
	
	//average();
	

	//worst();
	//demo();
	heapsort();
	
	profiler.showReport();
	cout << "the result";

}