#include"vec4.h"
namespace egc
{
	vec4& vec4::operator =(const vec4 &srcVector)
	{
		(*this).x = srcVector.x;
		(*this).y = srcVector.y;
		(*this).z = srcVector.z;
		(*this).w = srcVector.w;
		return *this;

	}
	vec4 vec4::operator +(const vec4& srcVector) const
	{
		return vec4(x + srcVector.x, y + srcVector.y, z + srcVector.z, w + srcVector.w);

	}
	vec4& vec4::operator +=(const vec4& srcVector)
	{
		this->x = (*this).x + srcVector.x;
		this->y = (*this).y + srcVector.y;
		this->z = (*this).z + srcVector.z;
		this->w = (*this).w + srcVector.w;
		return *this;
	}
	vec4 vec4::operator *(float scalarValue) const
	{
		return vec4(this->x*scalarValue, this->y*scalarValue, this->z*scalarValue, this->w*scalarValue);

	}
	vec4 vec4::operator -(const vec4& srcVector) const
	{
		return vec4(this->x - srcVector.x, this->y - srcVector.y, this->z - srcVector.z, this->w - srcVector.w);
	}
	vec4& vec4::operator -=(const vec4& srcVector) {
		this->x = (*this).x - srcVector.x;
		this->y = (*this).y - srcVector.y;
		this->z = (*this).z - srcVector.z;
		this->w = (*this).w - srcVector.w;
		return *this;


	}
	vec4& vec4::operator -() {
		this->x = -this->x;
		this->y = -this->y;
		this->z = -this->z;
		this->w = -this->w;
		return *this;
	}
	float vec4::length() const
	{
		return  sqrt(this->x*this->x + this->y*this->y + this->z*this->z+ this->w*this->w);
	}
	vec4& vec4::normalize() {
		float len = (*this).length();
		this->x = this->x / len;
		this->y = this->y / len;
		this->z = this->z / len;
		this->w = this->w / len;
		return *this;

	}
	float dotProduct(const vec4& v1, const vec4& v2)
	{
		return(v1.x*v2.x + v1.y*v2.y + v1.z*v2.z);
	}




	

}