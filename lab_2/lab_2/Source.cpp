#include<iostream>
#include "Profiler.h"
using namespace std;
Profiler profiler("demo");
// the insertio and bubble sort algorithms are stable but the selection one isn't 
//stable because it can break the order in which equal items appear eg 7,3,4,5,7,2->the first 7 will be swapped with
//the 2 and so it will break the order.
//Observation:Bubble sort is the most costy operation in every case
//Selection sort is in the middle at nr of comparisons and assignations and insertion sort is more efficient than the other two
// Complexity in the worst case:
//Bubble O(n^2)
//Selection O(n^2)
//Insertion O(n^2)
int s[10000],b[10000],in[10000];


int big_size=1000;
int size1;

void bubble()
{
	int repeat = 1;
	while (repeat==1)//repeat while unsorted completely
	{
		repeat = 0;
		for (int i = 1; i < size1 ; i++)
		{
			
			profiler.countOperation("Bubble_comps", size1, 1);
			if (b[i - 1] > b[i])//if an element is bigger than the next one
			{
				profiler.countOperation("Bubble_assigns", size1, 3);	//swap them
				int aux;
				aux = b[i - 1];
				b[i - 1] = b[i];
				b[i] = aux;
				repeat = 1;//signal that the array is unsorted
			}
			
		}
	}

}

void insertion()	//insertion sort
{
	for (int i = 1; i < size1; i++)
	{
		int x;
		x= in[i];		//take current item
		int j = i-1;
		while((j>-1) && (in[j]>x))	//go to the left until we find a spot for him

		{
			profiler.countOperation("Insertion Comparisons", size1, 1);
			profiler.countOperation("Insertion Assignations", size1, 1);
			in[j + 1] = in[j];	//shift right all elements
			j--;
		}
		profiler.countOperation("Insertion Comparisons", size1, 1);
		if (j + 1 != i)
		{
			profiler.countOperation("Insertion Assignations", size1, 1);
			in[j + 1] = x; //put it in the empty space
			
		}
	}


}
void selection()
{
	for (int i = 0; i < size1 - 1; i++)//loop through n-1
	{
		int index = i;
		for (int j = i + 1; j < size1; j++)
		{
			profiler.countOperation("Selection Comparisons", size1, 1);
			if (s[index] > s[j])//if elemnt at index j smaller than the one at index
			{	
				index = j;
			}

		}
		if (index != i) {//if found a smaller element
			{profiler.countOperation("Selection Assignations", size1, 3);
			int aux = s[index];
			s[index] = s[i];
			s[i] = aux;
			}
		}
	}

}
void generate_arrays()//copy the generated array into the other arrays
{
	for (int i = 0; i < size1; i++)
	{
		b[i] = s[i];
		in[i] = s[i];
	}

}
void best() {//best case generator and sorting
	for (int j = 100; j < big_size; j += 200)
	{
		size1 = j;
		FillRandomArray(s, j, 0, 1000000, 1, 1);
		generate_arrays();
		selection();
		bubble();
		insertion();

	}



	profiler.createGroup("Comparisons", "Bubble_comps", "Insertion Comparisons");
	profiler.createGroup("Assignations", "Bubble_assigns", "Insertion Assignations", "Selection Assignations");
	profiler.addSeries("Insertion Operations", "Insertion Comparisons", "Insertion Assignations");
	profiler.addSeries("Bubble Operations", "Bubble_comps", "Bubble_assigns");
	profiler.addSeries("Selection Operations", "Selection Comparisons", "Selection Assignations");
	profiler.createGroup("Total Number of operations", "Bubble Operations", "Insertion Operations", "Selection Operations");
	profiler.showReport();




}
void worst() {//worst case generator and sorter

	for (int j = 100; j < big_size; j += 200)
	{
		size1 = j;
		FillRandomArray(s, j, 0, size1, 1, 2);
		generate_arrays();
		for (int i = (size1-1)/2; i >= 0; i--)//half sorted descending
			s[size1 -i+1] = i;
		for (int i = size1/ 2; i < size1; i++)//half sorted ascending
			s[i] = i-size1/2;
		
		selection();
		bubble();
		insertion();

	}
	//das gut
	profiler.createGroup("Comparisons", "Bubble_comps", "Insertion Comparisons", "Selection Comparisons");
	profiler.createGroup("Assignations", "Bubble_assigns", "Insertion Assignations");
	profiler.addSeries("Insertion Operations", "Insertion Comparisons", "Insertion Assignations");
	profiler.addSeries("Bubble Operations", "Bubble_comps", "Bubble_assigns");
	profiler.addSeries("Selection Operations", "Selection Comparisons", "Selection Assignations");
	profiler.createGroup("Total Number of operations", "Bubble Operations", "Insertion Operations", "Selection Operations");
	profiler.showReport();

}
void average()//average case generator and sorter
{

	for (int k = 0; k < 5; k++)//loop 5 time to make an average
	{

		for (int j = 100; j < big_size; j += 200)
		{
			size1 = j;
			FillRandomArray(s, j, 0, 1000000, 1, 0);
			generate_arrays();
			selection();
			bubble();
			insertion();

		}


	}
	profiler.createGroup("Comparisons", "Bubble_comps", "Insertion Comparisons", "Selection Comparisons");
	profiler.createGroup("Assignations", "Bubble_assigns", "Insertion Assignations");
	profiler.addSeries("Insertion Operations", "Insertion Comparisons", "Insertion Assignations");
	profiler.addSeries("Bubble Operations", "Bubble_comps", "Bubble_assigns");
	profiler.addSeries("Selection Operations", "Selection Comparisons", "Selection Assignations");
	profiler.createGroup("Total Number of operations", "Bubble Operations", "Insertion Operations", "Selection Operations");
	profiler.showReport();
	

}

int main()
{//3 methods, uncomment one to view the graphic
	//best();
	//worst();
	average();

	cout << "Ready";
	}
