#include "mat3.h"
#include <algorithm>
#include <iostream>
#include "vec3.h"
#include "vec4.h"
#include "mat3.h"
#include "mat4.h"
#include "transform.h"

namespace egc
{
	mat3 translate(const vec2 translateArray)
	{
		float aux[] = { 1,0,0,0,1,0,translateArray.x,translateArray.y,1 };
		mat3 aux1(aux);

		return aux1;
	}
	mat3 translate(float tx, float ty)
	{
		float aux[] = { 1,0,0,0,1,0,tx,ty,1 };
		mat3 aux1(aux);

		return aux1;
	}
	mat3 scale(const vec2 scaleArray)
	{
		float aux[] = { scaleArray.x,0,0,0,scaleArray.y,0,0,0,1 };
		mat3 aux1(aux);

		return aux1;

	}
	mat3 scale(float sx, float sy)
	{
		float aux[] = { sx,0,0,0,sy,0,0,0,1 };
		mat3 aux1(aux);

		return aux1;
	}
	mat3 rotate(float angle)
	{
		float radians = 2 * PI * angle / 360;
		float aux[] = { cos(radians),
			sin(radians),
			0,
			-sin(radians),
			cos(radians),
			0,
			0,0,1 };
		mat3 aux1(aux);

		return aux1;

	}
	mat4 translate(const vec3 translateArray)
	{

		float aux[] = { 1,0,0,0,
			0,1,0,0,
			0,0,1,0,
			translateArray.x,translateArray.y,translateArray.z,1 };
		mat4 aux1(aux);

		return aux1;


	}
	mat4 translate(float tx, float ty, float tz)
	{

		float aux[] = { 1,0,0,0,
			0,1,0,0,
			0,0,1,0,
			tx,ty,tz,1 };
		mat4 aux1(aux);

		return aux1;
	}
	mat4 scale(const vec3 scaleArray)
	{
		float aux[] = { scaleArray.x,
			0,0,0,
			0,
			scaleArray.y,
			0,0,
			0,0,
			scaleArray.z,0,
			0,0,0,1 };

		mat4 aux1(aux);

		return aux1;


	}
	mat4 scale(float sx, float sy, float sz)
	{
		float aux[] = { sx,
			0,0,0,
			0,
			sy,
			0,0,
			0,0,
			sz,0,
			0,0,0,1 };

		mat4 aux1(aux);

		return aux1;


	}
	mat4 rotateZ(float angle)
	{
		float r = 2 * PI * angle / 360;
		float aux[] = {
			cos(r),
			sin(r),
			0,
			0,
			-sin(r),cos(r),0,0,
			0,0,1,0,
			0,0,0,1
		};
		mat4 aux1(aux);
		return aux1;
	}
	mat4 rotateX(float angle)
	{
		float r = 2 * PI * angle / 360;
		float aux[] = {
			1,0,0,0,
			0,cos(r),sin(r),0,
			0,-sin(r),cos(r),0,
			0,0,0,1 };

		mat4 aux1(aux);
		return aux1;

	}
	mat4 rotateY(float angle)
	{
		float r = 2 * PI * angle / 360;
		float aux[] = {
			cos(r),0,-sin(r),0,
			0,1,0,0,
			sin(r),0,cos(r),0,
			0,0,0,1
		};
		mat4 aux1(aux);
		return aux1;
	}
}