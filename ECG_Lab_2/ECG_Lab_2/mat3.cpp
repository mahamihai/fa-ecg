#include "mat3.h"
#include <algorithm>
#include <iostream>


namespace egc
{
	mat3& mat3::operator =(const mat3& srcMatrix)
	{
		for (int i = 0; i < 9; i++)
		{
			(*this).matrixData[i] = srcMatrix.matrixData[i];


		}
		return *this;
	}
	mat3 mat3::operator +(const mat3& srcMatrix) const
	{
		mat3 aux;
		for (int i = 0; i < 9; i++)
		{
			aux.matrixData[i] = (*this).matrixData[i] + srcMatrix.matrixData[i];

		}
		return aux;

	}
	mat3 mat3::operator *(const mat3& srcMatrix) const
	{
		mat3 aux;
		int i;
		for (i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				float x = 0;
				for (int k = 0; k < 3; k++)
				{
					x =x+ (*this).at(i,k)*srcMatrix.at(k ,j);
					std::cout << (*this).matrixData[i * 3 + k]<<" "<< srcMatrix.matrixData[k * 3 + j]<<" ";
				}
				std::cout << '\n';
				aux.at(i,j) = x;
				

			}
			



		}

		return aux;

	}
	const float& mat3::at(int i, int j) const
	{
		return (*this).matrixData[j * 3 + i];

	}
	float& mat3::at(int i, int j)
	{
		return (*this).matrixData[j * 3 + i];

	}
	
	vec3 mat3::operator *(const vec3& srcVector) const
	{
		vec3 aux;
		float t[3];
		int i;
		for (i = 0; i < 3; i++)
		{
			
					float x = 0;
				
					x+= (*this).at(i,0)*srcVector.x ;
					x += (*this).at(i, 1) * srcVector.y;
					x += (*this).at(i, 2) * srcVector.z;
					t[i] = x;

		}
		aux.x = t[0];
		aux.y = t[1];
		aux.z = t[2];

			std::cout << '\n';
	

	return aux;

	}
	mat3 mat3::transpose() const
	{
		mat3 aux;
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				aux.at(i, j) = (*this).at(j, i);

			}

		}
		return aux;

	}
	

	float mat3::determinant() const
	{
		float det1=0;
		det1+=(*this).at(0, 0)*((*this).at(1, 1)*(*this).at(2, 2) - (*this).at(2, 1)*(*this).at(1, 2))- (*this).at(0, 1)*((*this).at(1, 0)*(*this).at(2, 2) - (*this).at(1, 2)*(*this).at(2, 0))+ (*this).at(0, 2)*((*this).at(1, 0)*(*this).at(2, 1) - (*this).at(1, 1)*(*this).at(2, 0));

		return det1;

	}
	mat3 mat3::operator *(float scalarValue) const
	{
		
		mat3 aux;
		for (int i = 0; i < 9; i++)
		{
			aux.matrixData[i] = (*this).matrixData[i] * scalarValue;
	
		}
		return aux;


	}
}
