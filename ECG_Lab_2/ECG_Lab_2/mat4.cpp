#include <cmath>
#include <iostream>
#include "mat4.h"

namespace egc {

	float& mat4::at(int i, int j) {
		return this->matrixData[4 * j + i];
	}

	const float& mat4::at(int i, int j)const {
		return this->matrixData[4 * j + i];
	}

	mat4& mat4 :: operator =(const mat4& srcMatrix) {
		for (int i = 0; i < 16; i++) {
			this->matrixData[i] = srcMatrix.matrixData[i];
		}
		return *this;
	}

	mat4 mat4 :: operator *(float scalarValue)const {
		mat4 mat = mat4(*this);
		for (int i = 0; i < 16; i++) {
			mat.matrixData[i] = mat.matrixData[i] * scalarValue;
		}
		return mat;
	}

	mat4 mat4 :: operator *(const mat4& srcMatrix) const {
		mat4 mat = mat4(*this);
		for (int i = 0; i <= 3; i++) {
			for (int j = 0; j <= 3; j++) {
				float sum = 0;
				for (int k = 0; k <= 3; k++) {
					sum = sum + this->at(i, k)*srcMatrix.at(k, j);
				}
				mat.matrixData[4 * j + i] = sum;
			}
		}
		return mat;
	}

	vec4 mat4 :: operator *(const vec4& srcVector) const {
		vec4 v = vec4();
		v.x = this->matrixData[0] * srcVector.x + this->matrixData[4] * srcVector.y + this->matrixData[8] * srcVector.z + this->matrixData[12]*srcVector.w;
		v.y = this->matrixData[1] * srcVector.x + this->matrixData[5] * srcVector.y + this->matrixData[9] * srcVector.z + this->matrixData[13] * srcVector.w;
		v.z = this->matrixData[2] * srcVector.x + this->matrixData[6] * srcVector.y + this->matrixData[10] * srcVector.z + this->matrixData[14] * srcVector.w;
		v.w = this->matrixData[3] * srcVector.x + this->matrixData[7] * srcVector.y + this->matrixData[11] * srcVector.z + this->matrixData[15] * srcVector.w;

		return v;
	}

	mat4 mat4 :: operator +(const mat4& srcMatrix) const {
		mat4 mat = mat4(*this);
		for (int i = 0; i < 16; i++) {
			mat.matrixData[i] = mat.matrixData[i] + srcMatrix.matrixData[i];
		}
		return mat;
	}

	float mat4::determinant() const {
		float det = 0, sum1 = 0, sum2 = 0, sum3=0, sum4=0;

		sum1 = this->matrixData[0] * (this->matrixData[5] * this->matrixData[10] * this->matrixData[15] + this->matrixData[9] * this->matrixData[14] * this->matrixData[7] + this->matrixData[13] * this->matrixData[6] * this->matrixData[11] - this->matrixData[7] * this->matrixData[10] * this->matrixData[13] - this->matrixData[11] * this->matrixData[14] * this->matrixData[5] - this->matrixData[9] * this->matrixData[6] * this->matrixData[15]);
		sum2 = this->matrixData[1] * (this->matrixData[4] * this->matrixData[10] * this->matrixData[15] + this->matrixData[8] * this->matrixData[14] * this->matrixData[7] + this->matrixData[12] * this->matrixData[6] * this->matrixData[11] - this->matrixData[7] * this->matrixData[10] * this->matrixData[12] - this->matrixData[11] * this->matrixData[14] * this->matrixData[4] - this->matrixData[8] * this->matrixData[6] * this->matrixData[15]);
		sum3 = this->matrixData[2] * (this->matrixData[4] * this->matrixData[9] * this->matrixData[15] + this->matrixData[8] * this->matrixData[13] * this->matrixData[7] + this->matrixData[12] * this->matrixData[5] * this->matrixData[11] - this->matrixData[7] * this->matrixData[9] * this->matrixData[12] - this->matrixData[11] * this->matrixData[13] * this->matrixData[4] - this->matrixData[9] * this->matrixData[5] * this->matrixData[15]);
		sum4 = this->matrixData[3] * (this->matrixData[4] * this->matrixData[9] * this->matrixData[14] + this->matrixData[8] * this->matrixData[13] * this->matrixData[6] + this->matrixData[12] * this->matrixData[5] * this->matrixData[10] - this->matrixData[6] * this->matrixData[9] * this->matrixData[12] - this->matrixData[10] * this->matrixData[13] * this->matrixData[4] - this->matrixData[8] * this->matrixData[5] * this->matrixData[14]);
		
		det = sum1 - sum2 + sum3 - sum4;

		return det;
	}

	mat4 mat4::inverse() const {
		mat4 mat = mat4();
		float demp = 0;
		/*mat.at(0, 0) = this->matrixData[5] * this->matrixData[10] * this->matrixData[15] + this->matrixData[9] * this->matrixData[14] * this->matrixData[7] + this->matrixData[13] * this->matrixData[6] * this->matrixData[11] - this->matrixData[7] * this->matrixData[10] * this->matrixData[13] - this->matrixData[11] * this->matrixData[14] * this->matrixData[5] - this->matrixData[9] * this->matrixData[6] * this->matrixData[15];
		mat.at(1, 0) = (-1)*(this->matrixData[4] * this->matrixData[10] * this->matrixData[15] + this->matrixData[8] * this->matrixData[14] * this->matrixData[7] + this->matrixData[12] * this->matrixData[6] * this->matrixData[11] - this->matrixData[7] * this->matrixData[10] * this->matrixData[12] - this->matrixData[11] * this->matrixData[14] * this->matrixData[4] - this->matrixData[8] * this->matrixData[6] * this->matrixData[15]);
		mat.at(2, 0) = this->matrixData[4] * this->matrixData[9] * this->matrixData[15] + this->matrixData[8] * this->matrixData[13] * this->matrixData[7] + this->matrixData[12] * this->matrixData[5] * this->matrixData[11] - this->matrixData[7] * this->matrixData[9] * this->matrixData[12] - this->matrixData[11] * this->matrixData[13] * this->matrixData[4] - this->matrixData[9] * this->matrixData[5] * this->matrixData[15];
		mat.at(3, 0) = (-1)*(this->matrixData[4] * this->matrixData[9] * this->matrixData[14] + this->matrixData[8] * this->matrixData[13] * this->matrixData[6] + this->matrixData[12] * this->matrixData[5] * this->matrixData[10] - this->matrixData[6] * this->matrixData[9] * this->matrixData[12] - this->matrixData[10] * this->matrixData[13] * this->matrixData[4] - this->matrixData[8] * this->matrixData[5] * this->matrixData[14]);

		mat.at(0, 1) = (-1)*(this->matrixData[1] * this->matrixData[10] * this->matrixData[15] + this->matrixData[9] * this->matrixData[14] * this->matrixData[3] + this->matrixData[13] * this->matrixData[2] * this->matrixData[11] - this->matrixData[3] * this->matrixData[10] * this->matrixData[13] - this->matrixData[11] * this->matrixData[14] * this->matrixData[1] - this->matrixData[9] * this->matrixData[2] * this->matrixData[15]);
		mat.at(1, 1) = this->matrixData[0] * this->matrixData[10] * this->matrixData[15] + this->matrixData[8] * this->matrixData[14] * this->matrixData[3] + this->matrixData[12] * this->matrixData[2] * this->matrixData[11] - this->matrixData[3] * this->matrixData[10] * this->matrixData[12] - this->matrixData[11] * this->matrixData[14] * this->matrixData[0] - this->matrixData[8] * this->matrixData[2] * this->matrixData[15];
		mat.at(2, 1) = (-1)*(this->matrixData[0] * this->matrixData[9] * this->matrixData[15] + this->matrixData[8] * this->matrixData[13] * this->matrixData[3] + this->matrixData[12] * this->matrixData[1] * this->matrixData[11] - this->matrixData[3] * this->matrixData[9] * this->matrixData[12] - this->matrixData[11] * this->matrixData[13] * this->matrixData[0] - this->matrixData[8] * this->matrixData[1] * this->matrixData[15]);
		mat.at(3, 1) = this->matrixData[0] * this->matrixData[9] * this->matrixData[14] + this->matrixData[8] * this->matrixData[13] * this->matrixData[2] + this->matrixData[12] * this->matrixData[1] * this->matrixData[10] - this->matrixData[2] * this->matrixData[9] * this->matrixData[12] - this->matrixData[10] * this->matrixData[13] * this->matrixData[0] - this->matrixData[8] * this->matrixData[1] * this->matrixData[14];

		mat.at(0, 2) = this->matrixData[1] * this->matrixData[6] * this->matrixData[15] + this->matrixData[5] * this->matrixData[14] * this->matrixData[3] + this->matrixData[13] * this->matrixData[2] * this->matrixData[7] - this->matrixData[3] * this->matrixData[6] * this->matrixData[13] - this->matrixData[1] * this->matrixData[14] * this->matrixData[7] - this->matrixData[5] * this->matrixData[2] * this->matrixData[15];
		mat.at(1, 2) = (-1)*(this->matrixData[0] * this->matrixData[6] * this->matrixData[15] + this->matrixData[4] * this->matrixData[14] * this->matrixData[3] + this->matrixData[12] * this->matrixData[2] * this->matrixData[7] - this->matrixData[3] * this->matrixData[6] * this->matrixData[12] - this->matrixData[7] * this->matrixData[14] * this->matrixData[0] - this->matrixData[4] * this->matrixData[2] * this->matrixData[15]);
		mat.at(2, 2) = this->matrixData[0] * this->matrixData[5] * this->matrixData[15] + this->matrixData[4] * this->matrixData[13] * this->matrixData[3] + this->matrixData[12] * this->matrixData[1] * this->matrixData[7] - this->matrixData[3] * this->matrixData[5] * this->matrixData[12] - this->matrixData[7] * this->matrixData[13] * this->matrixData[0] - this->matrixData[4] * this->matrixData[1] * this->matrixData[15];
		mat.at(3, 2) = (-1)*(this->matrixData[0] * this->matrixData[5] * this->matrixData[14] + this->matrixData[4] * this->matrixData[13] * this->matrixData[2] + this->matrixData[12] * this->matrixData[1] * this->matrixData[6] - this->matrixData[2] * this->matrixData[5] * this->matrixData[12] - this->matrixData[6] * this->matrixData[13] * this->matrixData[0] - this->matrixData[4] * this->matrixData[1] * this->matrixData[14]);

		mat.at(0, 3) = (-1)*(this->matrixData[1] * this->matrixData[6] * this->matrixData[11] + this->matrixData[5] * this->matrixData[10] * this->matrixData[3] + this->matrixData[9] * this->matrixData[2] * this->matrixData[7] - this->matrixData[3] * this->matrixData[6] * this->matrixData[9] - this->matrixData[10] * this->matrixData[7] * this->matrixData[1] - this->matrixData[11] * this->matrixData[2] * this->matrixData[5]);
		mat.at(1, 3) = this->matrixData[0] * this->matrixData[6] * this->matrixData[11] + this->matrixData[4] * this->matrixData[10] * this->matrixData[3] + this->matrixData[8] * this->matrixData[2] * this->matrixData[7] - this->matrixData[3] * this->matrixData[6] * this->matrixData[8] - this->matrixData[7] * this->matrixData[10] * this->matrixData[0] - this->matrixData[11] * this->matrixData[2] * this->matrixData[4];
		mat.at(2, 3) = (-1)*(this->matrixData[0] * this->matrixData[5] * this->matrixData[11] + this->matrixData[4] * this->matrixData[9] * this->matrixData[3] + this->matrixData[8] * this->matrixData[1] * this->matrixData[7] - this->matrixData[3] * this->matrixData[5] * this->matrixData[8] - this->matrixData[7] * this->matrixData[9] * this->matrixData[0] - this->matrixData[11] * this->matrixData[1] * this->matrixData[4]);
		mat.at(3, 3) = this->matrixData[0] * this->matrixData[5] * this->matrixData[10] + this->matrixData[4] * this->matrixData[9] * this->matrixData[2] + this->matrixData[8] * this->matrixData[1] * this->matrixData[6] - this->matrixData[2] * this->matrixData[5] * this->matrixData[8] - this->matrixData[6] * this->matrixData[9] * this->matrixData[0] - this->matrixData[10] * this->matrixData[1] * this->matrixData[4];

		*/

		for (int i = 0;i < 16;i++)
		{
			for (int j = 0;j < 16;j++)
			{
				mat.at(j, i) = (this->at((i + 1) % 100, (+1) % 100)*
					this->at((i + 2) % 100, (j + 2) % 100) - (this->at((i + 1) % 100, (j + 2) % 100) *
						this->at((i + 2) % 00, (j + 1) % 100)) / this->determinant());
			}
			//float det = 1 / this->determinant();

			//mat = mat * det;

			return mat;
		}
	}
	mat4 mat4::transpose() const {
		mat4 mat = mat4();
		mat.at(0, 0) = this->at(0, 0);
		mat.at(0, 1) = this->at(1, 0);
		mat.at(0, 2) = this->at(2, 0);
		mat.at(0, 3) = this->at(3, 0);

		mat.at(1, 0) = this->at(0, 1);
		mat.at(1, 1) = this->at(1, 1);
		mat.at(1, 2) = this->at(2, 1);
		mat.at(1, 3) = this->at(3, 1);

		mat.at(2, 0) = this->at(0, 2);
		mat.at(2, 1) = this->at(1, 2);
		mat.at(2, 2) = this->at(2, 2);
		mat.at(2, 3) = this->at(3, 2);

		mat.at(3, 0) = this->at(0, 3);
		mat.at(3, 1) = this->at(1, 3);
		mat.at(3, 2) = this->at(2, 3);
		mat.at(3, 3) = this->at(3, 3);

		return mat;
	}
}