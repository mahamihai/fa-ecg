#include <stdio.h>
#include <fstream>
#include <cstdlib>
#include <stdlib.h>
#include "Profiler_linux.h"
#include <iostream>

#include <time.h>
Profiler profiler("numbers");
using namespace std;
int N=10007;
int cells=0;
int total_cells=0;
int max_cells=0;
int h2(int k)
{
	return N % k;
}
int hash_function(int i,int k)
{
return (h2(k)+4*i+7*(i*i))%N;

}
int hash_search(int *a,int k)
{
	 int j;
    int i=0;
    do{
        j=hash_function(i,k);
        if(k==a[j])
        {
        	cells++;

            return j;
        }
        cells++;

        i++;
    }
    while ((i!=N) && (a[j]!=-1));

    return -1;
}
int hash_insert(int *a,int key)
{int j;
	int i=0;
	do
	{
		j=hash_function(i,key);
		//cout <<j;
		if(a[j]==-1)
		{
			//cout<<"i is "<<i<<"\n";
			a[j]=key;
			return j;
		}
		else
			i++;


	}
	while (i!=N );
}
void print_array(int *a,int size)
{
	for (int i = 0; i < size; ++i)
	{
		cout<<a[i]<<" ";
	}
}
void generate_array(int *arr,int *inserted,double factor)
{
	
	for (int i = 0; i < N; ++i)
{
	arr[i]=-1;
}
	//print_array(arr,N);
for (int i = 0; i < factor; ++i)
{
	int random=rand()%10000+1;
	hash_insert(arr,random);
	inserted[i]=random;

    
}
//cout<<"\n\n\n";
//print_array(arr,N);

}
void search(int factor,int no_elems,int *arr,int *inserted)
{

	max_cells=0;
	int no_searches=3000;
	cells=0;
	total_cells=0;
	for(int i=0;i<no_searches/2;i++)
	{
		int j=rand()%no_elems;
		hash_search(arr,inserted[j]);
		total_cells+=cells;
		if(max_cells<cells)
		{
			max_cells=cells;
		}
		cells=0;

	}
	//cout<<"total "<<total_cells<<"\n";
	int total_found=total_cells;
	int max_found=max_cells;
	profiler.countOperation("Average Effort Found",factor,total_found/(no_searches/2));
	profiler.countOperation("Max Effort Found",factor,max_cells);
	
	max_cells=0;
	cells=0;
	total_cells=0;
	
	for(int i=0;i<no_searches/2;i++)
	{

		int j=rand()%10000+1;
		while(hash_search(arr,j)!=-1)
		{
			j=rand()%10000+1;
			cells=0;
		}
		
		
		total_cells+=cells;
		if(max_cells<cells)
		{
			max_cells=cells;
		}
		cells=0;

	}
	profiler.countOperation("Average Effort Not Found",factor,total_found/(no_searches/2));
	profiler.countOperation("Max Effort Not Found",factor,max_cells);
	//cout<<total_found/(no_searches/2)<<"  "<<max_cells<<"  ";

}
	
int main()
{
	cout<<"Started";
	srand(time(NULL));




//for (int )
double factors[5]={0.8,0.85,0.9,0.95,0.99};
for(int i=0;i<5;i++){
	int arr[N];
	double no_elems=factors[i]*N;

	int inserted[(int)no_elems];
	generate_array(arr,inserted,no_elems);
	
	search(i,(int)no_elems,arr,inserted);
}
profiler.createGroup("Searches","Average Effort Found","Max Effort Found","Average Effort Not Found","Max Effort Not Found");
profiler.showReport();
//cout<<hash_search(arr,);
//print_hash(inserted,(int)fill_factor);
//print_hash(arr,N);




}