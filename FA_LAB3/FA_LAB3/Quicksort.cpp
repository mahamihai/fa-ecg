#include <iostream>
#include <fstream>
#include <cstdlib>
#include "Profiler.h"

//quicksort with random pivot avoid the worst case
//quick is better than heapsort at nr of computations
//we cal fall into the worst case of quick only if we choose the pivot to be the min or max
//because we use the random pivot we can avoid the worst case and the average case will be closer to the best
Profiler profiler("heap");
using namespace std;

int n;
int numbers2;
int sir1[10000];
int heap_sorted[10000];
int max_size = 10000;

int numbers;//size

int partition(int *a,int p, int r){
	int x = a[r];
	int i = p - 1;
	for (int j = p ; j<r;j++)
	{
		if (a[j] <= x)
		{
			i++;
			int aux = a[i];
			a[i] = a[j];
			a[j] = aux;
			profiler.countOperation("Quick Assignations", numbers, 3);
			profiler.countOperation("Quick Comparisons", numbers, 1);
		}
		profiler.countOperation("Quick Comparisons", numbers, 1);
		


	}
	profiler.countOperation("Quick Assignations", numbers, 3);
	int aux = a[i + 1];
	a[i + 1] = a[r];
	a[r] = aux;
	return i + 1;
}
int partition2(int *a, int p, int r) {
	int x = a[(r + p + 1) / 2];
	a[(r + p + 1) / 2] = a[r];
	a[r] = x;
	int i = p - 1;
	for (int j = p; j<r; j++)
	{
		if (a[j] <= x)
		{
			i++;
			int aux = a[i];
			a[i] = a[j];
			a[j] = aux;
			profiler.countOperation("Quick Assignations", numbers, 3);
			profiler.countOperation("Quick Comparisons", numbers, 1);
		}
		profiler.countOperation("Quick Comparisons", numbers, 1);



	}
	profiler.countOperation("Quick Assignations", numbers, 3);
	int aux = a[i + 1];
	a[i + 1] = a[r];
	a[r] = aux;
	return i + 1;
}
int random_part(int *a,int p, int r)
{
	int i = (rand() % (r - p)) + p;
	int aux = a[r];
	a[r] = a[i];
	a[i] = aux;
	profiler.countOperation("Quick Assignations", numbers, 3);
	return partition(a,p, r);

}
int rand_select(int *a,int p,int r,int i)
{
	if (p==r)
	{
		return p;
	}
	int q=random_part(a,p,r);
	int k=q-p+1;
	if(i==k)
	{
		return q;
	}
	else
		if (i<k)
			return rand_select(a,p,q-1,i);
		else
			return rand_select(a,q+1,r,i-k);
}



void rand_quicksort(int *a,int p,int r)
{
	if (p < r)
	{
		int q = random_part(a,p, r);
		rand_quicksort(a,p, q - 1);
		rand_quicksort(a,q + 1, r);
	}
}
void quicksort(int *a,int p,int r)
{

	if (p < r)
	{
		int q = partition(a,p, r);
		quicksort(a,p, q - 1);
		quicksort(a,q + 1, r);
	}

}


void heapify(int *a,int index,int size)
{
	int pos=index;
	if ((index * 2 + 1)< size)
	{
		if (a[index * 2 + 1] > a[index])
		{
			pos = index * 2 + 1;

		}
		profiler.countOperation("Heapsort Comparisons", numbers, 1);

	}
	
	if ((index * 2 + 2) < size)
	{
		if (a[index * 2 + 2] > a[pos])
		{
			pos = index * 2 + 2;

		}
		
		profiler.countOperation("Heapsort Comparisons", numbers, 1);
	}
	if (pos != index)

	{

		profiler.countOperation("Heapsort Assignations", numbers, 3);
		int aux = a[index];
		a[index] = a[pos];
		a[pos] = aux;
		heapify(a,pos,size);

	}


}
void heap_bottom(int *a,int size)
{
	int index=size;


	for (int i = (size/2)-1; i>-1; i--)
	{


		heapify(a,i,size);



	}
}
void print_array(int *a,int index)
{
	for(int i=0;i<index;i++)
	{
		cout<<a[i]<<" ";
	}
}
void heap_sort(int *a,int size)
{
	heap_bottom(a,size);


	int index = size;
	for (int i = size-1; i >0; i--)
	{
		
		int aux=a[i];
		a[i] = a[0];
		a[0]=aux;
		
		profiler.countOperation("Heapsort Assignations",numbers,3);
		index--;
		
		heapify(a,0,index);
		
		
	}
	

}
	

void copy_array(int *a,int *b,int n)
{
	for(int i=0;i<n;i++)
	{
		a[i]=b[i];
	}
}

void best_quick(int *a, int p, int r)
{
	if (p < r)
	{
		int q = partition2(a, p, r);
		best_quick(a, p, q - 1);
		best_quick(a, q + 1, r);
	}


}

void demo_quick()
{
	int demo[20];
	FillRandomArray(demo,15,10,30,true,0);
	cout<<rand_select(demo,0,14,9);
	print_array(demo,15);
}
void average(int *a)
{
	for(int j=0;j<5;j++)
	{
	for(int i=100;i<10001;i+=200)
		{
			numbers=i;
	FillRandomArray(a, i, 10, 100000, false, 0);
	copy_array(sir1,a,i);

	rand_quicksort(a,0, i-1);
	heap_sort(sir1,i);
		
		
	}
}
	profiler.createGroup("Comparisons","Heapsort Comparisons","Quick Comparisons");
	profiler.createGroup("Assignations","Heapsort Assignations","Quick Assignations");
	profiler.addSeries("Heap Operations","Heapsort Comparisons","Heapsort Assignations");
	profiler.addSeries("Quick Operations","Quick Comparisons","Quick Assignations");
	profiler.createGroup("Total Operations","Heap Operations","Quick Operations");

	profiler.showReport();

}
void best(int *a)
{

	for (int i = 100; i < 10001; i += 200)
	{

		numbers = i;

		FillRandomArray(a, i, 10, 100000, true, 1);
		best_quick(a, 0, i - 1);
	}
	profiler.addSeries("Quick Operations", "Quick Comparisons", "Quick Assignations");

	profiler.showReport();

}
void worst(int *a)
{
	

	for(int i=100;i<10001;i+=200)
		{
			
	numbers=i;
	
	FillRandomArray(a, i, 10, 100000, true, 2);


	quicksort(a,0, i-1);
	
		
		
	}
	cout<<"Here";
	// profiler.createGroup("Comparisons","Heapsort Comparisons","Quick Comparisons");
	// profiler.createGroup("Assignations","Heapsort Assignations","Quick Assignations");
	// profiler.addSeries("Heap Operations","Heapsort Comparisons","Heapsort Assignations");
	 profiler.addSeries("Quick Operations","Quick Comparisons","Quick Assignations");
	// profiler.createGroup("Total Operations","Heap Operations","Quick Operations");

	profiler.showReport();


}


	int main()
	{
		int a[10001];
		FillRandomArray(a, 100, 0, 10000, true, 0);
		//average(a);
		//worst(a);
		best(a);
		//demo_quick();
		//cout << "Gata";
		//int r;
		//cout << rand_select(a, 0, 9, 4);
		//print_array(a,10000);
		//rand_quicksort(a,0,9999);
		//print_array(a,10000);
		int r;
		cin >> r;
	cout << "\n"<<"ready\n";
	}
