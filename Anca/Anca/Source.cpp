#include <iostream>
#include <stdio.h>
#include "Profiler.h"


using namespace std;
Profiler profiler("demo");

int heapsize = -1;

void max_heapify(int *a, int i, int n)
{
	int largest, l, r, aux;
	l = 2 * i + 1;
	r = 2 * i + 2;
	profiler.countOperation("Max_heap_CMP", n, 1);
	profiler.countOperation("Max_heap_OP", n, 1);
	if (l <= n && a[l] > a[i])
		largest = l;
	else largest = i;
	profiler.countOperation("Max_heap_CMP", n, 1);
	profiler.countOperation("Max_heap_OP", n, 1);
	if (r <= n && a[r] > a[largest])
		largest = r;
	if (largest != i)
	{
		profiler.countOperation("Max_heap_ASSIG", n, 3);
		profiler.countOperation("Max_heap_OP", n, 3);

		aux = a[i];
		a[i] = a[largest];
		a[largest] = aux;
		max_heapify(a, largest, n);
	}
}


void build_max_heap(int *a, int n)
{
	for (int i = (n / 2) - 1; i >= 0; i--)
		max_heapify(a, i, n);
}


void min_heapify(int *a, int i, int key, int n)
{
	int aux;
	profiler.countOperation("Min_heap_ASSIG", n, 1);
	profiler.countOperation("Min_heap_OP", n, 1);

	a[i] = key;

	if (a[i / 2] >= a[i])
	{
		profiler.countOperation("Min_heap_CMP", n, 1); // in case it doesn`t enter in the while loop
		profiler.countOperation("Min_heap_OP", n, 1);
	}

	while (i > 0 && a[i / 2] < a[i])
	{
		profiler.countOperation("Min_heap_CMP", n, 1);
		aux = a[i / 2];
		a[i / 2] = a[i];
		a[i] = aux;
		profiler.countOperation("Min_heap_ASSIG", n, 3);
		profiler.countOperation("Min_heap_OP", n, 4);
		i = i / 2;
	}
}

void build_min_heap(int *a, int key, int n)
{
	heapsize++;
	a[heapsize] = INT_MIN;
	profiler.countOperation("Min_heap_OP", n, 1);
	profiler.countOperation("Min_heap_ASSIG", n, 1);
	min_heapify(a, heapsize, key, n);

}


void main()
{
	//int v[] = { 1,4,3,2,5,6,7,40,32 };
	//int s[20];
	//int n = sizeof(v) / sizeof(v[0]);

	//for (int i = 0; i < n; i++)
	//{
	//	build_min_heap(s, v[i], n);
	//	//build_min_heap(v, v[i], n);
	//	
	//}
	//for (int i = 0; i < n; i++)
	//	cout << s[i]<<" ";

	int v[10000], s[10000], t[10000];
	for (int n = 100; n < 10000; n += 100)
	{  // AVERAGE CASE 
		FillRandomArray(v, n, 0, 1000, true, 0);
		for (int i = 0; i<n; i++)
			s[i] = v[i];
		for (int i = 0; i < n; i++)
			build_min_heap(t, v[i], n);

		build_max_heap(s, n);


		profiler.createGroup("Heap_AVERAGE_ASSIG", "Max_heap_ASSIG", "Min_heap_ASSIG");
		profiler.createGroup("Heap_AVERAGE_CMP", "Max_heap_CMP", "Min_heap_CMP");
		profiler.createGroup("Heap_AVERAGE_OP", "Max_heap_OP", "Min_heap_OP");

	}


	profiler.showReport();







}

