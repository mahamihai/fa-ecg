#include <stdio.h>
#include <fstream>
#include <cstdlib>
#include <stdlib.h>
#include "Profiler_linux.h"
#include <iostream>
     /* printf, scanf, puts, NULL */
   /* srand, rand */
#include <time.h>
using namespace std;
int numbers;
int key;
Profiler profiler("heap");
using namespace std;

 typedef struct nod  
{
	int value;
	int list;
	 nod *next;
}element;
nod *insert_element(  nod *root, int element,int index)
{
	 nod *add =(nod*) malloc(sizeof(nod));
    add->list=index;
    add->next=NULL;
    add->value=element;
	 nod *aux = root;
	if (root == NULL)
	{
		return add;
	}
	else
	{
		while(root && root->next != NULL)
		{
		    root=root->next;
		}
		
		root->next=add;

	}
	return aux;

}
void print_list(nod *root)
{
        nod *aux = root;

		while ((aux) != NULL)
		{
			cout<<((*aux).value)<<" ";
			aux = (*aux).next;
		}
		
	

}

void heapify(nod **a, int index, int size)
{
	int pos = index;
	if ((index * 2 + 1)< size)
	{
		if (a[index * 2 + 1]->value < a[index]->value)
		{
			pos = index * 2 + 1;

		}
		profiler.countOperation("Merge Comparisons", numbers, 1);

	}

	if ((index * 2 + 2) < size)
	{
		if (a[index * 2 + 2]->value < a[pos]->value)
		{
			pos = index * 2 + 2;

		}

        profiler.countOperation("Merge Comparisons", numbers, 1);
	}
	if (pos != index)

	{

        profiler.countOperation("Merge Assignations", numbers, 3);
		nod* aux = a[index];
		a[index] = a[pos];
		a[pos] = aux;
		heapify(a, pos, size);

	}


}
void heap_bottom(nod **a, int size)
{
	int index = size;


	for (int i = (size / 2) - 1; i>-1; i--)
	{


		heapify(a, i, size);



	}
}
void sort_k(nod** lists,int n,int k) {
    nod **heap = (nod **) malloc(sizeof(nod *) * k);
    int sorted[n]={0};
    for (int i = 0; i < k; i++) {
        nod *aux=lists[i];
        heap[i] = lists[i];
        lists[i]=lists[i]->next;


        profiler.countOperation("Merge Assignations", numbers, 2);
    }

    heap_bottom(heap,k);

    for(int i=0;i<n;i++)
    {

        heapify(heap,0,k);
       sorted[i]=heap[0]->value;
        profiler.countOperation("Merge Assignations", numbers, 1);
//        cout<<"\nPrinting hipified:";
//    for (int j = 0; j < k; ++j) {
//        cout<<heap[j]->value<<" ";
//    }
        int queue=heap[0]->list;

        int loops=1;
        while((lists[queue]==NULL)&&loops!=k)
        {
            loops++;
            queue=(queue+1)%k;
            profiler.countOperation("Merge Comparisons", numbers, 1);
        }
        profiler.countOperation("Merge Comparisons", numbers, 1);


        if(loops==k)

        {
            heap[0]=heap[k-1];
            k--;
            profiler.countOperation("Merge Assignations", numbers, 1);
        }
        else {


            heap[0] = lists[queue];
            lists[queue] = lists[queue]->next;
            profiler.countOperation("Merge Assignations", numbers, 2);
        }
        //free(aux);
    }
//    cout<<"\n printing sorted";
//    for(int i=0;i<n;i++)
//    {
//        cout<<sorted[i]<<" ";
//    }


}
nod **generate(int n,int k)
{
    nod** lists=(nod**)malloc(k*sizeof(nod*));
   int sizes[k]={0};
    int t;
    for(int i=0;i<n;i++)
    {
       while(sizes[(t=rand()% k)]>=n/k){

       }

        sizes[t]++;
        lists[t]=insert_element(lists[t],i,t);


    }
    return lists;
}
void deallocate(nod **list,int k)
{
    for (int i = 0; i < k; ++i) {
        nod *aux=list[i];
        nod *freable=NULL;
        while(aux!=NULL)
        {
            freable=aux;
            aux=aux->next;
            free(freable);
        }
    }
}
int get_divider(int n,int k)
{
    for(int i=k+1;i<n;i++)
    {
        if((n % i)==0)
        {
            return i;
        }
    }
    return n;
}
int main() {
    srand(time(NULL));


    //int k = 25;
    
//
//    for (int i = 100; i < 10000; i += 200) {
//        numbers = i;
//        nod **list = NULL;
//        list = generate(i, k);
//        nod **freeable = list;
//        sort_k(list, i, k);
//        deallocate(list, k);
//
//
//    }

    int n=10000;
     int k=get_divider(n,9);
     for (; k <210; k=get_divider(n,k)) {
        numbers = n;
        key=k;
        nod **list = NULL;
        list = generate(n, k);
        nod **freeable = list;
        sort_k(list, n, k);
        deallocate(freeable, k);
        cout<<k<<" ";

    }
    profiler.showReport();


	return 0;
}