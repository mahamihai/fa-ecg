#include <stdio.h>
#include <fstream>
#include <cstdlib>
#include <stdlib.h>
#include "Profiler_linux.h"
#include <iostream>

#include <time.h>
using namespace std;
int numbers;
int key;
Profiler profiler("numbers");
Profiler profiler1("key");
using namespace std;

typedef struct nod
{
    int value;
    int list;
    nod *next;
}element;
nod *insert_element(  nod *root, int element,int index)
{
    nod *add =(nod*) malloc(sizeof(nod));
    add->list=index;
    add->next=NULL;
    add->value=element;
    nod *aux = root;
    if (root == NULL)
    {
        return add;
    }
    else
    {
        while(root!=NULL && root->next != NULL)
        {
            root=root->next;
        }

        root->next=add;

    }
    return aux;

}
void print_list(nod *root)
{
    nod *aux = root;

    while ((aux) != NULL)
    {
        cout<<((*aux).value)<<" ";
        aux = (*aux).next;
    }
    cout<<"\n";



}

void heapify(nod **a, int index, int size,char *kappa)
{
    int pos = index;
    if ((index * 2 + 1)< size)
    {
        if (a[index * 2 + 1]->value < a[index]->value)
        {
            pos = index * 2 + 1;

        }
        profiler.countOperation(kappa, numbers, 1);
        profiler1.countOperation("Merge Comparisons", key, 1);


    }

    if ((index * 2 + 2) < size)
    {
        if (a[index * 2 + 2]->value < a[pos]->value)
        {
            pos = index * 2 + 2;

        }

        profiler.countOperation(kappa, numbers, 1);
        profiler1.countOperation("Merge Comparisons", key, 1);

    }
    if (pos != index)

    {

        profiler.countOperation(kappa, numbers, 3);
        profiler1.countOperation("Merge Assignations", key, 3);

        nod* aux = a[index];
        a[index] = a[pos];
        a[pos] = aux;
        heapify(a, pos, size,kappa);

    }


}
void heap_bottom(nod **a, int size,char *kappa)
{
    int index = size;


    for (int i = (size / 2) - 1; i>-1; i--)
    {


        heapify(a, i, size,kappa);



    }
}
void sort_k(nod** lists,int n,int k,char *kappa) {
    nod **heap = (nod **) malloc(sizeof(nod *) * k);
    int sorted[n]={0};

    for (int i = 0; i < k; i++) {
        nod *aux=lists[i];
        heap[i] = lists[i];
        lists[i]=lists[i]->next;



        profiler.countOperation(kappa, numbers, 2);
        profiler1.countOperation("Merge Assignations", key, 2);

    }

    heap_bottom(heap,k,kappa);

    for(int i=0;i<n;i++)
    {

        heapify(heap,0,k,kappa);
        sorted[i]=heap[0]->value;
        profiler.countOperation(kappa, numbers, 1);
        profiler1.countOperation("Merge Assignations", key, 1);

//        cout<<"\nPrinting hipified:";
//    for (int j = 0; j < k; ++j) {
//        cout<<heap[j]->value<<" ";
//    }
        int queue=heap[0]->list;

        int loops=1;
        while((lists[queue]==NULL)&&loops!=k)
        {
            loops++;
            queue=(queue+1)%k;

        }
        profiler.countOperation(kappa, numbers, 1);
        profiler1.countOperation("Merge Comparisons", key, 1);

        if(loops==k)

        {
            heap[0]=heap[k-1];
            k--;
            profiler.countOperation(kappa, numbers, 1);
            profiler1.countOperation("Merge Assignations", key, 1);

        }
        else {


            heap[0] = lists[queue];
            lists[queue] = lists[queue]->next;
            profiler.countOperation(kappa, numbers, 2);
            profiler1.countOperation("Merge Assignations", key, 2);

        }
        //free(aux);
    }
    cout<<"\n printing sorted";
    for(int i=0;i<n;i++)
    {
        cout<<sorted[i]<<" ";
    }


}
nod **generate(int n,int k)
{
    nod** lists=(nod**)malloc(k*sizeof(nod*));
    for(int i=0;i<k;i++)
    {
        lists[i]=NULL;
    }
    int sizes[k]={0};
    int t;

    for(int i=0;i<n;i++)
    {
        while(sizes[(t=rand()% k)]>=(n/k+n%k)){

        }

        sizes[t]++;
        lists[t]=insert_element(lists[t],i,t);


    }
//    for(int i=0;i<k;i++)
//        print_list(lists[i]);
    return lists;
}
void deallocate(nod **list,int k)
{
    for (int i = 0; i < k; ++i) {
        nod *aux=list[i];
        nod *freable=NULL;
        while(aux!=NULL)
        {
            freable=aux;
            aux=aux->next;
            free(freable);
        }
    }
}
int get_divider(int n,int k)
{
    for(int i=k+1;i<n;i++)
    {
        if((n % i)==0)
        {
            return i;
        }
    }
    return n;
}
void demo()
{
    char kappa[100]="";
    int i=20;
    int k=3;
    nod **list = NULL;
    list = generate(i, k);
    for(int t=0;t<k;t++)
    {
        print_list(list[t]);
        cout<<"\n";
    }

    nod **freeable = list;
    sort_k(list, i, k,kappa);
    deallocate(freeable, k);

}
void average(){
     int n=10000;
    char kappa[100]="";
      for (int k=10; k < 500; k=10+k) {
         numbers = n;
         key=k;
         nod **list = NULL;
         list = generate(n, k);
         nod **freeable = list;
         sort_k(list, n, k,kappa);
         deallocate(freeable, k);
         cout<<k<<" ";

     }

 profiler1.addSeries("Toal ops","Merge Assignations","Merge Comparisons");

    profiler1.showReport();

}
void k_fixed()
{


    int k = 5;
    char kappa[100]="5";
    strcpy(kappa,"5");
    for(;k<101;) {
        for (int o = 0; o < 5; o++) {
            key = k;
            for (int i = 100; i < 10000; i += 200) {
                numbers = i;
                nod **list = NULL;
                list = generate(i, k);
                nod **freeable = list;
                sort_k(list, i, k,kappa);
                deallocate(freeable, k);

            }
        }
        if (k == 5) {
            strcpy(kappa,"10");
            k = 10;
        } else if (k == 10) {
            strcpy(kappa,"100");
            k = 100;
        } else if (k == 100) {
            k++;

        }
    }

    //profiler.addSeries("Toal ops","Merge Assignations","Merge Comparisons");
    profiler.createGroup("Total ops","5","10","100");
    profiler.showReport();

}
int main() {
    srand(time(NULL));
    demo();
    //k_fixed();
//average();
    cout<<"\nReady";
    return 0;
}